package com.marina.empresas;

import android.util.Log;

import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Marinoca on 06/08/2017.
 */

public class Autentificacao {

    private Usuario usuario;
    private Retrofit retrofit;
    private final EmpresaServidor empresaServidor;
    final LoginActivity login;

    public Autentificacao(LoginActivity login){

        this.login = login;

        ChamarRetrofit chamarRetrofit = new ChamarRetrofit();
        retrofit = chamarRetrofit.getRetrofit();
        empresaServidor = retrofit.create(EmpresaServidor.class);
    }

    public void Logar (String email, String senha){

        usuario = new Usuario(email, senha);

        Call call = empresaServidor.confirmar(usuario);

        call.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {

                if(response.isSuccessful()){

                    UsuarioResposta usuarioResposta = new UsuarioResposta(response.headers().get("access-token"),
                            response.headers().get("client"),
                            response.headers().get("uid"));

                    Log.d("access-token",response.headers().get("access-token"));
                    Log.d("client", response.headers().get("client"));
                    Log.d("uid", response.headers().get("uid"));
                    login.Chamar(usuarioResposta);

                } else {

                    login.mensagemErro(login.getString(R.string.errologin));
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {

                login.mensagemErro(t.getMessage());

            }
        });

    }
}
