package com.marina.empresas;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Marinoca on 06/08/2017.
 */

public interface EmpresaServidor {

    @POST("users/auth/sign_in")
    Call<Usuario> confirmar(@Body Usuario usuario);

    @GET("enterprises")
    Call<EmpresaLista> empresaLista(
            @Header("access-token") String acess_token,
            @Header("client") String client,
            @Header("uid") String uid,
            @Query("name") String empresa);
}
