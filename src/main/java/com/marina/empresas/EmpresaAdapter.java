package com.marina.empresas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Marinoca on 06/08/2017.
 */

public class EmpresaAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Empresa> empresas;
    private LayoutInflater layoutInflater;

    public EmpresaAdapter(ArrayList<Empresa> empresas, Context context){
        this.context = context;
        this.empresas = empresas;
        this.layoutInflater = (LayoutInflater.from(context));
    }

    @Override
    public int getCount(){
        return empresas.size();
    }

    @Override
    public long getItemId(int position){
        return 0;
    }

    @Override
    public Object getItem(int position){
        return empresas.get(position);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent){
        view = layoutInflater.inflate(R.layout.lista_empresas,null);

        Empresa empresa = empresas.get(position);

        TextView nomeEmpresa;
        nomeEmpresa = (TextView) view.findViewById(R.id.nome_empresa);
        nomeEmpresa.setText((empresa.getEnterpriseName()));

        TextView tipoEmpresa;
        tipoEmpresa = (TextView) view.findViewById(R.id.tipo_empresa);
        tipoEmpresa.setText(empresa.getEnterpriseType().getEnterprise_type_name());

        TextView paisEmpresa;
        paisEmpresa = (TextView) view.findViewById(R.id.pais_empresa);
        paisEmpresa.setText(empresa.getCountry());

        return view;
    }

}
