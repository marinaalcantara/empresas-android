package com.marina.empresas;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private ImageView home;
    private TextView textoHome, textoHome2, email, senha;
    private Button entrar;
    private Autentificacao autentificacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);

        home = (ImageView) findViewById(R.id.login_imagemIoasys);

        email = (TextView) findViewById(R.id.edit_email);

        senha = (TextView) findViewById(R.id.edit_senha);


        Button entrar = (Button) findViewById(R.id.btLogin);
        entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                autentificacao = new Autentificacao(LoginActivity.this);
                autentificacao.Logar(email.getText().toString(),senha.getText().toString());
            }
        });

    }

    public void Chamar(UsuarioResposta usuarioResposta){
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        intent.putExtra("user", usuarioResposta);
        Toast.makeText(this, "Bem-vindo! ", Toast.LENGTH_SHORT).show();

        startActivity(intent);
    }

    public void mensagemErro(String erro){
        Toast.makeText(getBaseContext(), erro ,Toast.LENGTH_LONG).show();
    }

}



