package com.marina.empresas;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Body;

/**
 * Created by Marinoca on 06/08/2017.
 */

public class BuscarEmpresas {

    private Retrofit retrofit;
    private UsuarioResposta usuarioResposta;
    private HomeActivity homeActivity;
    private EmpresaServidor empresaServidor;

    public BuscarEmpresas(UsuarioResposta usuarioResposta, HomeActivity homeActivity){
        ChamarRetrofit chamarRetrofit = new ChamarRetrofit();
        retrofit = chamarRetrofit.getRetrofit();
        this.empresaServidor = retrofit.create(EmpresaServidor.class);
        this.usuarioResposta = usuarioResposta;
        this.homeActivity = homeActivity;
    }

    public void EncontrarEmpresas(String s, final HomeActivity homeActivity){

        if(usuarioResposta != null){
            Call call = empresaServidor.empresaLista(usuarioResposta.getAccessToken(),
                    usuarioResposta.getClient(),
                    usuarioResposta.getUid(), s);

            call.enqueue(new Callback<EmpresaLista>() {
                @Override
                public void onResponse(Call<EmpresaLista> call, Response<EmpresaLista> response) {

                    Log.d("Body", response.body().toString());
                    homeActivity.procurar(response.body());
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    homeActivity.mensagemErro(t.getMessage());
                }
            });
        }
    }
}
