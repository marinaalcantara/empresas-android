package com.marina.empresas;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    private TextView texto;
    private ListView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        texto = (TextView) findViewById(R.id.textView);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.backgraud_menu));

        View view = getLayoutInflater().inflate(R.layout.imagem,null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                Gravity.CENTER);
        actionBar.setCustomView(view,params);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

        lista = (ListView) findViewById(R.id.lista_empresas);

    }


    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_home, menu);

        UsuarioResposta usuarioResposta =  (UsuarioResposta) getIntent().getSerializableExtra("user");
        final BuscarEmpresas buscarEmpresas = new BuscarEmpresas(usuarioResposta,HomeActivity.this);
        final MenuItem myActionMenuItem = menu.findItem(R.id.pesquisar);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (! searchView.isIconified()) {
                    buscarEmpresas.EncontrarEmpresas(query,HomeActivity.this);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }

        });
        return true;
    }

    public void mensagemErro(String erro) {
        Toast.makeText(getBaseContext(), erro, Toast.LENGTH_LONG).show();
    }

    public void procurar(final EmpresaLista empresaLista){

        final ArrayList<Empresa> empresas = empresaLista.getEmpresas();

        if(empresas.size() == 0){
            Toast.makeText(this,"Nenhuma empresa encontrada!", Toast.LENGTH_SHORT).show();
        } else {
            texto.setVisibility(View.INVISIBLE);

            final EmpresaAdapter empresaAdapter = new EmpresaAdapter(empresas, getBaseContext());
            lista.setAdapter(empresaAdapter);

            lista.setVisibility(View.VISIBLE);

            lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(HomeActivity.this, EmpresaActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("nome", empresas.get(position).getEnterpriseName());
                    bundle.putString("descricao", empresas.get(position).getDescription());

                    i.putExtras(bundle);
                    startActivity(i);
                }
            });

        }
    }
}

