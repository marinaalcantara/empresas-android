package com.marina.empresas;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

public class EmpresaActivity extends AppCompatActivity {

    private String descricaoEmpresa;
    private TextView descricao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresa);

        final ActionBar actionBar = getSupportActionBar();

        descricao = (TextView) findViewById(R.id.descricaoEmpresa);
        descricaoEmpresa = getIntent().getStringExtra("descricao").toString();
        descricao.setText(descricaoEmpresa);

        final ActionBar actionBar2 = getSupportActionBar();
        actionBar2.setBackgroundDrawable(getResources().getDrawable(R.drawable.backgraud_menu));
        actionBar2.setTitle(getIntent().getStringExtra("nome").toString());
        actionBar2.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){

        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        finish();
        return true;
    }
}
