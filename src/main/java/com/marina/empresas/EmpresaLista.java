package com.marina.empresas;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Marinoca on 06/08/2017.
 */

public class EmpresaLista implements Serializable {

    @SerializedName("enterprises")
    private ArrayList<Empresa> empresas;

    public EmpresaLista(){
        this.empresas = new ArrayList<Empresa>();
    }

    public ArrayList<Empresa> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(ArrayList<Empresa> empresas) {
        this.empresas = empresas;
    }

    @Override
    public String toString(){
        String resposta="";
        for (Empresa e: empresas){
            resposta = resposta + e.getEnterpriseName()+"\n";
        }
        return resposta;
    }
}
