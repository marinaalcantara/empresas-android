package com.marina.empresas;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Marinoca on 06/08/2017.
 */

public class Usuario {

    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String senha;

    public Usuario(String email, String senha){
        this.email = email;
        this.senha = senha;
    }

    public String getEmail(){
        return email;
    }
    public String getSenha(){
        return senha;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
